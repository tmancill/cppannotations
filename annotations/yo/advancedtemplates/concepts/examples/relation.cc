#include <concepts>
#include <iostream>
#include <string>

template <typename LHS, typename RHS>
    requires std::totally_ordered_with<LHS, RHS>    // multiple ? combine in
auto pred =                                         // a separate concept
    [](LHS lhs, RHS rhs)
    {
        return lhs < rhs;
    };


template <typename LHS, typename RHS>
bool smaller(LHS lhs, RHS rhs)
    requires std::totally_ordered_with<LHS, RHS>
{
    return lhs < rhs;
}

template <typename Fun,
          typename LHS, typename RHS>
bool cmp3(Fun fun, LHS lhs, RHS rhs)
    requires std::relation<Fun, LHS, RHS>
{
    return fun(lhs, rhs);
}

template <typename T1, typename T2>
T1 fun(T2)
{
    return T1{};
}

template <typename LHS, typename RHS>
bool cmp2(LHS lhs, RHS rhs)
    requires std::relation<decltype(pred<LHS, RHS>), LHS, RHS>
{
    return pred<LHS, RHS>(lhs, rhs);
}

//relation
template <typename LHS, typename RHS>
struct Less
{
    bool operator()(LHS lhs, RHS rhs) const
    {
        return lhs < rhs;
    }
};

template <template<typename LHS, typename RHS> typename Pred,
          typename LHS, typename RHS>
bool cmp(LHS lhs, RHS rhs)
    requires std::relation<Pred<LHS, RHS>, LHS, RHS>
{
    return Pred<LHS, RHS>{}(lhs, rhs);
}

int main()
{
    std::cout << cmp <Less>(5, 4.9) << '\n';
    std::cout << cmp <Less>(5, "hello world") << '\n';
}
//=


//
//int main()
//{
//    std::cout <<
//        cmp2(3, 4)       << ' ' <<
//        cmp2(3.2, 4)     << ' ' <<
//        cmp2(3, 4.2)     << ' ' <<
//        cmp2(3.3, 4.4)   << ' ' <<
//        cmp < Less >(5, 4) << ' ' <<
//        cmp3( smaller<int, int>, 5, 4) << ' ' <<
//    '\n';
//
//    int x = fun<int>(std::string{});
//    x += 12;
//}
//
