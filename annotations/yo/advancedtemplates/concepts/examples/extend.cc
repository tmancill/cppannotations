template<Addable Type>
    requires Subtractable<Type>
Type addsub(Type const &x, Type const &y, Type const &z)
{
    return x + y - z;
}
