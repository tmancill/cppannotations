Consider the situation where different threads of a multi-threaded program
must write to the same file. The information written by each thread should
show up as a single block in that file. There are several ways to solve this
problem: each thread could write to a global file that's associated with just
one thread, and by the time the threads have stopped all these files are
copied to the destination file. Alternatively, the destination file could be
passed to the threads, while each thread defines its own local file, writing
its information to that file. Then, by the time the thread is about to end it
locks access to the destination file, and copies its local file to the
destination file. 

Recently the class tt(std::osyncstream) was added to the language, allowing
multi threaded programs allowing threads to write information block-wise to a
common stream without having to define separate streams receiving the
thread-specific information, eventually copying those streams to the
destination stream. Before using tt(osyncstream) objects the tthi(syncstream)
header file must be included.

The tt(osyncstream) class publicly inherits from tt(std::ostream),
initializing the tt(ostream) base class with a tt(std::syncbuf) stream buffer
(described in the next section), which performs the actual synchronization.

Information written to tt(osyncstream) objects can explicitly be copied to a
destination tt(ostream), or is automatically copied to the destination
tt(ostream) by the tt(osyncstream's) destructor. Each thread may construct its
own tt(osyncstream) object, handling the block-wise copying of the
information it receives to the destination stream.

bf(Constructors)

    itemization(
    itt(osyncstream{ostream &out}) constructs an tt(osyncstream) object
        eventually writing the information it receives to tt(out). Below,
        tt(out) is called the em(destination stream);
    itt(osyncstream{osyncstream &&tmp}) the move constructor is available;
    )

    The default- and copy-constructors are not available.

bf(Member functions)

    In addition to the members inherited from tt(std::ostream) (like the
tt(rdbuf) member returing a pointer to the object's tt(syncbuf) (described in
the next section)) the class tt(osyncstream) offers these members:
    itemization(
    iti(get_wrapped), returning a pointer to the destination stream's stream
        buffer;
    iti(emit), copies the received information as a block to the destination
        stream.
    )

The following program illustrates how tt(osyncstream) objects can be used.
    verbinsert(-n //code examples/osyncstream/main.cc)
    itemization(
    it() The function tt(fun) (line 8) is called by tt(main) from two threads
        (lines 27, 28);
    it() It defines an tt(osyncstream out) and, using short one-second pauses,
        writes some lines of text to tt(out) (lines 14, 15);
    it() Just before leaving tt(fun) the local tt(out) content is written
        as a block to tt(cout) (line 18). Writing tt(out's) content to
        tt(cout) can also explicitly be requested by calling tt(out.emit()).
    )


