What if, instead of returning tt(std::suspend_always State's) members return
tt(std::suspend_never)? In that case the coroutine, once it has started, is is
never suspended. If the program computing fibonacci numbers is then called
with argument 2, the following happens:
    itemization(
    it() at line 2 the program starts;
    it() at line 3 it looks as though tt(fiboCoroutine) is called,
        but before that: 
       itemization(
        itt(State::get_return_object) is called, returning a tt(Fibo) object
            at line 3.
        it() immediately after constructing the tt(Fibo) object the
            coroutine's execution continues, as this time
            tt(Fibo::State::initial_suspend) doesn't suspend. The tt(fibo)
            object, however, has been constructed since tt(auto fibo = ...)
            isn't an assignment but an initialization of the tt(fibo) object.
        )
    it() Since the coroutine's execution isn't suspended, it starts to
        iterate, and so it calls tt(co_yield). But although
        tt(Fibo::State::yield_value) is called at tt(co_yield) calls the
        coroutine isn't suspended, as tt(yield_value) now returns
        tt(suspend_never).
    it() So the coroutine continues its loop, assigning the next fibonacci
        number to tt(State::d_value) at each subsequent iteration.  Since the
        loop isn't suspended and since there's no other exit from the
        loop, the program continues until it's terminated by some signal (like
        ctrl-C).
    )
